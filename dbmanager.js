"use-strict";
class Backup{
	constructor(a){
		this.in=a;
	}
	get blob(){
		return new Promise((resolver, rechazar)=>{
			this.array.then(a=>{
				resolver(new Blob(a));
			});
		});
	}
	get array(){
		return window.db.transaction("r", window.db.estudio, ()=>{
			return window.db.estudio.toCollection().toArray(a=>{
				return a;
			});
		});
	}
}
class Restaurar{
	constructor(a){
		if(window.db.isOpen()){
			this.db=window.db;
		}
		this.in=a;
		this.eventos={};
	}
	on(a, b){
		this.eventos[a]=b;
	}
	restaurar(){
		var db=this.db;
		db.delete();
		db=undefined;
		let cons=new BaseDatos({db: this.in.nombre});
		cons.Fabrir().then(db=>{
			window.db=db;
			db.transaction("rw", db.estudio, ()=>{
				return db.estudio.bulkAdd(this.in.db);
			}).then(a=>{
				if(this.eventos.rsfin!=undefined){
					this.eventos.rsfin(true);
				}
			}).catch(e=>{
				this.eventos.rsfin(false);
				console.error(e);
			});
		});
	}
}
class Combinar{
	constructor(a){
		this.in={
			db: a.db
		}
		this.db=a.conn;
		this.eventos={};
	}
	on(a, b){
		this.eventos[a]=b;
	}
	restaurar(){
		var db=this.db;
		db.transaction("rw", db.estudio, ()=>{
			for(let i=0; i<this.in.db.length; i++){
				db.estudio.add(this.in.db[i]);
			}
		}).then(()=>{
			if(this.eventos.rsfin!=undefined){
				this.eventos.rsfin(true);
			}
		}).catch(e=>{
			console.error(e);
			this.err=e;
			this.eventos.rsfin(false);
		});
	}
}
