"use-strict";
class Archivo{
	constructor(a){
		this.in={
			archivos: a.archivos
		};
		this.out={};
	}
	get coded(){
		return new Promise((aceptar, rechazar)=>{
			var archivos=[];
			if(this.in.archivos.length==0){
				aceptar(archivos);
			}
			var interno=this.in;
			for(let i=0; i< this.in.archivos.length; i++){
				let reader = new FileReader();
				reader.onloadend=function(a){
					archivos[i]={nombre: interno.archivos[i].name, tipo: interno.archivos[i].type, contenido: a.target.result};
					if(archivos.length==interno.archivos.length){
						aceptar(archivos);
					}
				}
				reader.readAsArrayBuffer(this.in.archivos[i]);
			}
		});
	}
}
class BaseDatos{
	constructor(a){
		this.in={
			db: a.db
		};
		this.out={};
	}
	Fabrir(){
		return new Promise((resolver, rechazar)=>{
			var t1=Date.now();
			let a = new Dexie(this.in.db);
			a.version(1).stores({
				estudio: "++id,desc,&adjuntos"
			});
			a.open().then(()=>{
				console.log("Base de datos abierta en",(Date.now()-t1),"ms");
				resolver(a);
			});
		});
	}
}
function añadir(a){
	var a=a;
	return new Promise((resolver, rechazar)=>{
		let descripción= a.vars.desc;
        if(!descripción||descripción=="")throw new Error("La descripción no puede estar vacía");
		let adjuntos= new Archivo({archivos: a.vars.archivos});
		adjuntos.coded.then(b=>{
			a.db.estudio.put({
				desc: descripción,
				adjuntos: b
			}).then(c=>{
				resolver(c);
			}).catch(e=>{
				rechazar(e);
			});
		}).catch(e=>{
			console.error(e);
		});
	});
}
class Vik{
	constructor(a){
		this.in={a};
		this.out={};
	}
	static Get(a){
		if(a!=undefined){
			var a=a;
		}else{
			var a=this.in.url;
		}
		return new Promise((resolver, rechazar)=>{
			$.get(a, datos=>{
				resolver(datos);
			});
		});
	}
}
class Ajustes{
	constructor(a){
		this.in=a;
		this.out={};
	}
	static Aplicar(){
		//Búsqueda automática
		$("#ajustes-búsqueda_automática").prop("checked", window.vars.autobusqueda);
		return;
	}
}
class Búsqueda{
	constructor(a){
		this.in=a;
		this.out={};
	}
	get resultados(){
		return this.Fbuscar();
	}
	Fbuscar(){
		"use-strict";
		var interno=this.in;
		var este=this;
		interno.vars.expresión=this.F2regex(this.in.vars.expresión);
		return new Promise((resolver, rechazar)=>{
			var resultado=[];
			interno.db.estudio.toCollection().toArray(a=>{
				let b=este.Ffiltrar({filtro: interno.vars.expresión, posibles: a});
				resolver(b);
			});
		});
	}
	Ffiltrar(a){
		var resultado=[];
		for(let i=0; i<a.posibles.length; i++){
			a.posibles[i].desc.toLowerCase().match(a.filtro) && resultado.push(a.posibles[i])
		}
		return resultado;
	}
	F2regex(a){
		let salida;
		let pre=a.toLowerCase();
		function rx(b){
			return RegExp(b, "g");
		}
		pre=pre.replace(rx("a"), "[áa]");
		pre=pre.replace(rx("e"), "[ée]");
		pre=pre.replace(rx("i"), "[íi]");
		pre=pre.replace(rx("o"), "[óo]");
		pre=pre.replace(rx("u"), "[úu]");
		salida="("+pre+")";
		return RegExp(salida, "g");
	}
}
class Resultado{
	constructor(a){
		this.in=a;
		this.out={};
	}
	get página(){
		var plantilla=Handlebars.compile($(this.in.plantilla).html());
		this.out.plantilla=plantilla;
		var interno=this;
		return new Promise((resolver, rechazar)=>{
			interno.in.db.estudio.where("id").equals(interno.in.id).each(a=>{
				resolver(interno.Fparse(a));
			});
		});
	}
	Fparse(a){
		let plantilla=this.out.plantilla;
		return plantilla(a);
	}
}
class Comprobar{
	static sync(a){
		return sha256(a);
	}
	static async(a){
		return new Promise((resolver, rechazar)=>{
			resolver(this.sync(a));
		});
	}
}
class Spin{
	constructor(a){
		this.id=Number(String(Math.random()).substr(2, 4));
		this.elementos={
			html: "<div class='animación-carga' style='display: none;' data-spin-id='"+this.id+"'></div>",
			objetivo: $(a)
		}
		this.elementos["spinner"]=this.elementos.objetivo.append(this.elementos.html).children("[data-spin-id='"+this.id+"']");
		this.elementos.spinner.animate({
			height: Number($(window).height() * 0.05),
			width: Number($(window).height() * 0.05)
		}, 2);
	}
	iniciar(){
		this.elementos.spinner.show(100, "linear");
	}
	parar(){
		this.elementos.spinner.hide(100, "linear");
	}
}
