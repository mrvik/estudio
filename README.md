# Búsqueda de notas de estudio

## Preguntas frecuentes:

### ¿Qué es este programa?
- En este programa se pueden introducir archivos y descripciones de estos
- Después de introducirlos es posible buscarlos en la base de datos
- Ahora se puede sincronizar el contenido de la base de datos con otro dispositivo
- En un futuro no muy lejano será posible hacer backups de la base de datos

### ¿Dónde se guardan las notas?
- Usamos técnicas de vanguardia como la IDBFactory de los navegadores para guardar y posteriormente buscar la información
- Si haces un bakup tú decides dónde guardarlo :wink:

### ¿Cómo es de rápida esta app?
- Como sucede la mayoría de las veces, los desarrolladores tratamos de hacerla lo más rápida y sencilla posible, por lo que debería ser rápida, pero todo depende del dispositivo que use el usuario

### Cuando sincronizo mis notas con otro dispositivo, ¿Es seguro?
- En el sentido de la privacidad es perfectamente seguro, el navegador cuando carga la página crea una conexión cifrada mediante https con el servidor de WebSockets [(Ver proyecto)](https://github.com/mrvik/socket_io) , que redirige la información hacia el destino que hayas marcado. Todas las comunicaciones se hacen de manera segura y no es posible alterarlas.
  - #### Ese servidor, ¿qué información guarda?
    - Como es posible ver en el [proyecto](https://github.com/mrvik/socket_io) solo se guarda la información necesaria para comunicar los dos clientes. Cuando se desconectan el servidor destruye la variable donde guarda la conexión. Además la ID con la que se identifica un dispositivo es transitoria y cada vez se le asigna una al azar
- En el sentido de la integridad de la base de datos al ser transferida de un sitio a otro, es muy improbable que falle, sin embargo, como las conexiones a internet son muy antojadizas, hemos añadido una comprobación a la base de datos para asegurarnos de que sea seguro transferir los datos de un sitio a otro. Esto resta velocidad al proceso, pero con esta medida pretendemos que la seguridad del usuario esté garantizada

### En este proyecto usamos algunas librerías, ¿Qué es una librería?
- Una librería es un conjunto de procedimientos que hacen que los proramadores escribamos menos líneas de código, la app vaya más rápido y los usuarios usen la app sin problemas.

### ¿Qué librerías usamos aquí?

Librería | Utilidad
----------|---------
jQuery | Aquí podemos modificar la página a medida que el usuario va interaccionando con ella. Cuando pulsas un botón quieres que funcione ¿no?
Twitter-Bootstrap | ¿Te gusta el estilo de la página? Es similar en el ordenador, en el móvil, en la tablet, en la pantalla del microondas... Pues esto es gracias a Bootstrap
Dexie | Es como un :rocket: para la IDBFactory de tu navegador y como un :sunglasses: para nosotros
HandleBars.js | Cuando haces una búsqueda, pedimos a Dexie la base de datos, la filtramos y HandleBars te presenta los resultados como un :candy:
Vex.js | Qué prefieres, un :ice_cream: o un :shit: Pues VEX nos ayuda a presentarte los mensajes como un :ice_cream:
Socket.io | Cuando no sincronizas tu base de datos es un :sleeping: pero en cuanto eliges un destino para sincronizar agarra tu base de datos y se la lleva en :bullettrain_side: a la velocidad del :dash:
js-sha256 | Para prometerte que la base de datos que sincronices no va a corromperse necesitamos esta librería que le da una capa extra de :lock:
