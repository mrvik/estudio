'use strict';
//MASTER BRANCH
//ServiceWorker 2017.07.24 Production

var cacheName="Estudio-2017.07.24";
var cacheList=[];
var urlsToCache=[
	"cont/ajustes.html",
	"cont/añadir.html",
	"cont/información.html",
	"cont/información-modificar.html",
	"cont/resultados.html",
	"cont/sincronización.html",
	"cont/sincro-resultados.html"
];
var ignoreUrls=["websockets"];
function notin(a, b, c){
	for(let i=0; i<b.length; i++){
		if(c==true){
			if(a==b[i]){
				return false;
			}
		}else{
			if(a.match(b)){
				return false;
			}
		}
	}
	return true;
}
this.addEventListener("install", event=>{
	console.log(">> Instalando ServiceWorker");
	event.waitUntil(
		caches.open(cacheName).then(cache=>{
			return cache.addAll(urlsToCache);
		})
	);
});
///////////////////////////////////
this.addEventListener("fetch", event=>{
	event.respondWith(
		caches.match(event.request).then(response=>{
			return response || fetch(event.request).then(response=>{
				return caches.open(cacheName).then(cache=>{
					if(event.request.url.match(/(http([s]|):\/\/)/g)){
						if(notin(event.request.url, ignoreUrls)){
							cache.put(event.request, response.clone());
						}
					}
					return response;
				});
			});
		})
	);
});
/////////////////////////////////
this.addEventListener("activate", event=>{
	console.log(">> Activando ServiceWorker");
	event.waitUntil(
		caches.keys().then(list=>{
			return Promise.all(list.map(key=>{
				if(cacheList.indexOf(key) === -1){
					return caches.delete(key);
				}
			}));
		})
	);
});
/////////////////////////////////
this.addEventListener("message", event=>{
	var msg=event.data;
	console.log(msg);
	mhandler.tipo[msg.tipo](msg.contenido).then(a=>{
		event.ports[0].postMessage(a);
	}).catch(e=>{
		console.error(e);
	});
});
var mhandler={
	tipo:{
		importante: function(a){
			return new Promise(function(aceptar, rechazar){
				mhandler.ordenes[a.orden](a.datos).then(b=>{
					aceptar(b);
				}).catch(e=>{
					rechazar(e);
				});
			});
		}
	},
	ordenes:{
		recargar: function(a){
			return new Promise(function(aceptar, rechazar){
				return caches.open(cacheName).then(cache=>{
					cache.delete(a).then(()=>{
						cache.add(a).then(()=>{
							console.log("La cache ha sido modificada correctamente");
							aceptar({tipo: "respuesta", contenido: {datos: "ok"}});
						});
					});
				});
			});
		},
		eliminarcache: function(a){
			return caches.keys().then(list=>{
				return Promise.all(list.map(key=>{
					return caches.delete(key);
				}));
			});
		}
	}
};
