class Sincronización{
	constructor(a){
		this.in=a;
		//Preparamos el entorno
		Vik.Get("cont/sincronización.html").then(b=>{
			$("#visor").show(200).html(b);
			this.spinner=new Spin("#sincro-estado-animación");
			this.estado("Abriendo conexión con el servidor");
			Eventos.sincro();
			if(socketctl.socket.connected){
				$(".sincro-id").text(socketctl.socket.id);
				this.estado();
			}
			socketctl.socket.on('connect', ()=>{
				$(".sincro-id").text(socketctl.socket.id);
				this.estado();
			});
			socketctl.abrir();
		});
	}
	buscar(){
		this.estado("Buscando posibles conexiones...");
		socketctl.discover().then(a=>{
			if(a.length==0){
				$("#sincro-dispositivos").html("<h4>No hay dispositivos disponibles</h4>");
				this.estado();
				return;
			}
			let plantilla=Handlebars.compile($("#plantilla-sincro").html());
			$("#sincro-dispositivos").show(200).html(plantilla(a));
			Eventos.sincro_resultados();
			this.estado();
		});
	}
	iniciar(a){
		this.estado("Enviando mensaje...");
		socketctl.enviar({tipo: "init", dest: a, msg:""});
		this.estado("Esperando contestación...");
	}
	estado(a){
		if(a==undefined){
			this.spinner.parar();
			$("#sincro-estado-desc").html("");
		}else{
			this.spinner.iniciar();
			$("#sincro-estado-desc").text(a);
		}
	}
}
