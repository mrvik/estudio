$(window).on("load", ()=>{
	Variables.inicio();
	Eventos.principal();
	if(navigator.onLine){
		window.sw=new swctl({swname: "sw.js"});
	}
	let a= new BaseDatos({db: "Estudio"});
	a.Fabrir().then(b=>{
		window["db"]=b;
	}).catch(e=>{
		console.error(e);
	});
	Vik.Get("cont/resultados.html").then(a=>{
		$("#plantilla-resultados").html(a);
	});
	Vik.Get("cont/información.html").then(a=>{
		$("#plantilla-información").html(a);
	});
	Vik.Get("cont/información-modificar.html").then(a=>{
		$("#plantilla-información-modificar").html(a);
	});
	Vik.Get("cont/sincro-resultados.html").then(a=>{
		$("#plantilla-sincro").html(a);
	});
	if(navigator.mimeTypes.length==0){
		if(vars.mimeAlert!=true){
			vex.dialog.alert("Acabamos de detectar que tu dispositivo no reconoce ningún tipo de archivo, es posible que tengas problemas a la hora de ver los documentos");
			localStorage.setItem("mimeAlert", true);
		}
	}else{
		localStorage.removeItem("mimeAlert");
	}
	$("#main").show(200);
});
class Variables{
	constructor(){
		return window;
	}
	static inicio(){
		window["vars"]={};
		$.each(localStorage, (k, v)=>{
			window.vars[k]=new Util({parse: v}).Fparse();
		});
	}
}
class Eventos{
	constructor(){
		return document;
	}
	static principal(){
		$("#principal-info_mimes").on("click", ()=>{
			let salida;
			if(new Mimes().extensiones.length==0){
				salida="Oh oh, tu navegador no soporta ningún archivo! La app funcionará, pero deberás descargar los archivos que quieras ver";
			}else{
				salida="Estos son los archivos que soporta tu navegador:"+new Mimes().extensionesComoString;
			}
			vex.dialog.alert(salida);
		});
		$("#principal-sincro").on("click", ()=>{
			window["sincro"]=new Sincronización();
		});
		$("#principal-ajustes").on("click", ()=>{
			Vik.Get("cont/ajustes.html").then(a=>{
				$("#visor").html(a);
				Ajustes.Aplicar();
				this.ajustes();
				$("#visor").show(200);
			}).catch(e=>{
				console.error(e);
			});
		});
		$("#principal-añadir").on("click", ()=>{
			Vik.Get("cont/añadir.html").then(a=>{
				if($("#visor").css("display")!="none"){
					$("#visor").hide(200);
				}
				$("#visor").html(a);
				$("#visor").show(200);
				this.añadir();
			});
		});
		$("#principal-búsqueda").on({
			keyup: event=>{
				var a=$("#principal-búsqueda").val();
				if((!window.vars.autobusqueda || a.length<=0) && event.key!="Enter"){
					$("#visor").empty();
					return;
				}
				var t1=Date.now();
				var buscador=new Búsqueda({db: db, vars: {expresión: a}});
				buscador.resultados.then(b=>{
					let c= new Util({
						resultados: b,
						plantilla: "#plantilla-resultados"
					});
					let d=c.r2html;
					$("#visor").html(d);
					$("#visor").show(200);
					this.resultados();
					(Date.now()-t1) >= 800 && console.log("El tiempo excede el máximo ", Date.now()-t1);
					if(Date.now()-t1 >= 800){
						vex.dialog.alert("El tiempo de búsqueda ha excedido el límite. Se desactivará la búsqueda automática");
						localStorage.setItem("autobusqueda", false);
						Variables.inicio();
					}
				});
			},
			keydown: event=>{
				if(event.key=="Enter"){
					$("#principal-buscar").click();
				}
			}
		});
		$("#principal-buscar").on({
			click: ()=>{
				let a=$("#principal-búsqueda").val();
				var buscador= new Búsqueda({db: db, vars: {expresión: a}});
				buscador.resultados.then(b=>{
					let c= new Util({
						resultados: b,
					 plantilla: "#plantilla-resultados"
					});
					let d=c.r2html;
					$("#visor").hide(200).empty();
					$("#visor").html(d);
					$("#visor").show(200);
					this.resultados();
				});
			}
		});
		window["socketctl"]=new Socket();
	}
	static ajustes(){
		//Búsqueda automática
		$("#ajustes-búsqueda_automática").on("change", ()=>{
			let a = $("#ajustes-búsqueda_automática");
			localStorage.setItem("autobusqueda", a.is(":checked"));
			window.vars["autobusqueda"]=a.is(":checked");
		});
		return;
	}
	static añadir(){
		$("#añadir-confirmar").on("click", ()=>{
			añadir({
				db: db,
				vars: {
					archivos: document.querySelector("#añadir-adjunto").files,
					desc: $("#añadir-descripción").val()
				}
			}).then(()=>{
				$("#visor").hide(200).empty();
			}).catch(e=>{
				console.warn("Necesitarás la información de abajo para subir un reporte de error a GitLab");
				console.error(e);
                vex.dialog.alert(e.message);
			});
		});
	}
	static resultados(){
		$("[data-resultados-id]").on("click", event=>{
			var res=new Resultado({
				db: db,
				id: Number($(event.target).attr("data-resultados-id")),
				plantilla: "#plantilla-información"
			});
			let a = res.página;
			var id=$(event.target).attr("data-resultados-id");
			a.then(b=>{
				$("#visor").clearQueue().hide(200).empty().html(b).show(200);
				$("#información-editar").attr("data-información-id", id);
				this.información();
			});
		});
	}
	static información(){
		$("[data-información-adjunto]:not([data-información-adjunto-eliminar])").each((k, v)=>{
			var este=$(v);
			if(este.length==0){
				return;
			}
			var id=Number($("[data-información-id]").attr("data-información-id"));
			db.estudio.where("id").equals(id).each((a)=>{
				for(let i=0; i<a.adjuntos.length; i++){
					if(a.adjuntos[i].nombre==este.attr("data-información-adjunto")){
						var handler= new Blobs({
							archivo: a.adjuntos[i]
						});
						let regx=new Mimes().comoRegex;
						if(a.adjuntos[i].tipo.match(regx)==null){
							este.attr("data-download", handler.url);
							este.attr("data-download-name", a.adjuntos[i].nombre);
							este.attr("data-download-tipo", a.adjuntos[i].tipo);
							este.removeAttr("target");
							este.on("click", event=>{
								download(este.attr("data-download"), este.attr("data-download-name"), este.attr("data-download-type"));
							});
						}else{
							este.attr("href", handler.url);
						}
						break;
					}
				}
			});
		});
		$("#información-editar").on("click", ()=>{
			var id =Number($("[data-información-id]").attr("data-información-id")); 
			let res=new Resultado({
				db:db,
				id: id,
				plantilla: "#plantilla-información-modificar"
			});
			res.página.then(a=>{
				$("#visor").html(a).show(200);
				$("[data-información-adjunto]").attr("data-información-id", id);
				this.información();
			});
		});
		$("#información-modificar").on("click", event=>{
			if($(event.target).attr("data-información-id")==undefined){
				var tgt=$(event.target).parent();
			}else{
				var tgt=$(event.target);
			}
			var id=Number(tgt.attr("data-información-id"));
			var opciones={
				desc: $("#información-modificar_desc").val(),
				adjuntos: []
			};
			let a=new Archivo({
				archivos: document.querySelector("#información-añadir_archivos").files
			});
			a.coded.then(b=>{
				db.estudio.where("id").equals(id).modify(c=>{
					//Limpieza del array
					while(c.adjuntos.indexOf(undefined)!== -1){
						c.adjuntos.splice(c.adjuntos.indexOf(undefined), 1);
					}
					while(c.adjuntos.indexOf(null)!== -1){
						c.adjuntos.splice(c.adjuntos.indexOf(null), 1);
					}
					c.desc=opciones.desc;
					c.adjuntos=c.adjuntos.concat(b);
					$("#visor").hide(200);
				}).then(()=>{
					var id =Number($("[data-información-id]").attr("data-información-id"));
					let res=new Resultado({
						db: db,
						id: id,
						plantilla: "#plantilla-información"
					});
					res.página.then(a=>{
						$("#visor").html(b).show(200);
						$("#información-editar").attr("data-información-id", id);
						Eventos.información();
					});
				});
			});
		});
		$("[información-eliminar_adjunto]").on("click", async event=>{
            event.preventDefault();
            event.stopPropagation();
			if($(event.target).attr("data-información-adjunto")==undefined){
				var tgt=$(event.target).parent();
			}else{
				var tgt=$(event.target);
			}
			let res=await new Promise(resolver=>{
                vex.dialog.confirm({
                    message: "El archivo se eliminará para siempre",
                    callback: respuesta=>{
                        if(!respuesta){
                            return resolver(false);
                        }
                        return resolver(true);
                    }
                })
            });
            console.log(res);
            if(!res)return;
            let adj=tgt.attr("data-información-adjunto");
            return await db.transaction("rw", db.estudio, async ()=>{
                let col=db.estudio.where("id").equals(Number($("#información-modificar").attr("data-información-id")));
                return await col.modify(a=>{
                    for(let i in a.adjuntos){
                        if(!a.adjuntos.hasOwnProperty(i))continue;
                        let prop=a.adjuntos[i];
                        if(prop.nombre == adj){
                            a.adjuntos[i]=undefined;
                            return delete a.adjuntos[i];
                        }
                    }
                });
            }).then(X=>{
                $("dt.adjuntos[data-información-nombre='"+adj+"']").hide(200);
                tgt.hide(200);
            });
		});
		$("#información-eliminar").on("click", ()=>{
			vex.dialog.confirm({
				message: "La información se eliminará para siempre de la base de datos local",
				callback: a=>{
					if(a){
						$("#visor").hide();
						db.estudio.where("id").equals(Number($("[data-información-id]").attr("data-información-id"))).delete().then(()=>{
							$("#visor").empty();
						});
					}
				}
			});
		});
	}
	static socket(socket){
		socket.on("msg", a=>{
			console.log(a);
		});
		socket.on("direct", a=>{
			vex.dialog.alert("Mensaje del SocketServer: "+a);
		});
		socket.on("rst", a=>{
			vex.dialog.alert("La conexión ha terminado: "+a.msg);
			sincro.estado();
		});
		socket.on("init", a=>{
			vex.dialog.confirm({
				message: "¿Permitir que "+a.de+" se conecte y envíe su base de datos?",
				callback: b=>{
					if(b){
						socketctl.enviar({tipo: "init2", dest: a.de, msg: a});
						sincro.estado("Esperando base de datos...");
					}else{
						socketctl.enviar({tipo: "rst", dest: a.de, msg: "Conexión rechazada por el usuario"});
					}
				}
			});
		});
		socket.on("init2", a=>{
			console.log("Recibido el init2");
			vex.dialog.confirm({
				message: "Cuando pulses Ok leeremos el contenido de la base de datos y lo enviaremos. Por favor, no pierdas la conectividad",
				callback: b=>{
					if(!a) return;
					$("#sincro-dispositivos").html("");
					sincro.estado("Leyendo base de datos local...");
					let bkp=new Backup("Estudio");
					bkp.array.then(c=>{
						let d=Comprobar.sync(c);
						sincro.estado("Enviando base de datos...");
						socketctl.enviar({tipo: "database", dest: a.de, msg: {db:c, chk:d}});
						socket.on("status", a=>{
							if(a){
								sincro.estado();
								vex.dialog.alert("Base de datos sincronizada con éxito");
								socketctl.enviar({tipo: "fin", dest: a.de, msg: "!"});
								socketctl.terminar();
								$("#visor").hide(200);
							}
						});
					}).catch(e=>{
						console.error(e);
						vex.dialog.alert("Ha ocurrido un fallo al enviar la base de datos. Si ya te ha pasado más veces haz un reporte de fallos en github");
						//Manejamos el error
					});
				}
			});
		});
		socket.on("database2", ()=>{
			sincro.estado("Base de datos recibida por el otro dispositivo, esperando respuesta");
		});
		socket.on("dbok", ()=>{
			sincro.estado("Base de datos transferida, esperando a que el otro dispositivo la reciba");
		})
		socket.on("database", a=>{
			socketctl.enviar({tipo: "database2", dest: a.de, msg: true});
			socket.on("fin", ()=>{
				sincro.estado();
				socketctl.terminar();
				$("#visor").hide(200);
			});
			vex.dialog.buttons.YES.text="Reemplazar";
			vex.dialog.buttons.NO.text="Combinar";
			vex.dialog.buttons.NO.className="vex-dialog-button-primary";
			vex.dialog.confirm({
				message: "Acabamos de recibir la base de datos del otro dispositivo. ¿Qué quieres hacer con la actual?",
				callback: b=>{
					try{
						sincro.estado("Comprobando la base de datos");
						vex.dialog.buttons.YES.text="OK";
						vex.dialog.buttons.NO.text="Cancel";
						if(Comprobar.sync(a.msg.db)!=a.msg.chk){
							sincro.estado();
							vex.dialog.alert("La verificación de la base de datos ha fallado");
							throw new Error("La base de datos no se corresponde con el checksum");
							return;
						}
						sincro.estado("Escribiendo base de datos en el navegador");
						let rs;
						if(b){
							rs=new Restaurar({db: a.msg.db, nombre: "Estudio"});
						}else{
							rs= new Combinar({db: a.msg.db, conn: window.db});
						}
						rs.on("rsfin", b=>{
							sincro.estado("Base de datos introducida correctamente, enviando respuesta afirmativa al otro dispositivo");
							vex.dialog.alert("La base de datos se ha actualizado correctamente");
							socketctl.enviar({tipo: "status", dest: a.de, msg: b});
						});
						rs.restaurar();
					}catch(e){
						console.error(e);
						socketctl.enviar("status", false);
					}
				}
			});
		});
	}
	static sincro(){
		//Esta función solo debe usarse dentro del contexto del handler de sincronización
		$("#sincro-buscar").on("click", ()=>{
			sincro.buscar();
		});
	}
	static sincro_resultados(){
		$(".sincro-resultados-btn").on("click", event=>{
			var a= $(event.target);
			var id=a.attr("data-sincro-id");
			$("#sincro-dispositivos").html("");
			sincro.iniciar(id);
		});
	}
}
