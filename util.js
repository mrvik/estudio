class Util{
	constructor(a){
		this.in=a;
		this.out={};
	}
	Fparse(){
		let a = this.in.parse;
		if(a=="true"){
			return true;
		}else if(a=="false"){
			return false;
		}else{
			return a;
		}
	}
	get r2html(){
		let plantilla= this.Fhtml2hb();
		var entrada=this.in;
		var contextos={resultados: []};
		for(let i=0; i<entrada.resultados.length; i++){
			let contexto={
				Descripción: entrada.resultados[i].desc,
				archivos: entrada.resultados[i].adjuntos,
				ID: entrada.resultados[i].id
			};
			contextos.resultados.push(contexto);
		}
		return plantilla(contextos);
	}
	Fhtml2hb(){
		return Handlebars.compile($(this.in.plantilla).html());
	}
	indexOfProp(a, b, c){
		//Array Propiedad ValorEsperado
		for(let i=0; i<a.length; i++){
			if(a[i][b]==c){
				return i;
			}
		}
		return -1;
	}
}
class Mimes{
	get comoArray(){
		let salida=[];
		for(let i=0; i<navigator.mimeTypes.length; i++){
			salida.push(navigator.mimeTypes[i].type);
		}
		salida.push("image/.*"); //Sabemos que los navegadores soportan las imágenes
		return salida;
	}
	get comoRegex(){
		let salida="";
		let a=this.comoArray;
		for(let i=0; i<a.length; i++){
			if(i!=0){
				salida+="|";
			}
			salida+="("+this.comoArray[i]+")";
		}
		return RegExp(salida, "g");
	}
	soportado(a){
		
	}
	get extensiones(){
		let salida=[];
		for(let i=0; i<navigator.mimeTypes.length; i++){
			navigator.mimeTypes[i].suffixes!="" && salida.push(navigator.mimeTypes[i].suffixes);
		}
		return salida;
	}
	get extensionesComoString(){
		let salida="";
		for(let i=0; i<this.extensiones.length; i++){
			salida+=" "+this.extensiones[i];
		}
		return salida;
	}
}
