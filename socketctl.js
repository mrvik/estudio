"use-strict";
class Socket{
	constructor(a){
		let id=window.vars.peerID;
		this.socket= io("https://websockets-server.herokuapp.com/", {autoConnect: false});
		if(window.conexiones==undefined){
			window.conexiones={};
		}
		Eventos.socket(this.socket);
	}
	abrir(){
		this.socket.open();
	}
	enviar(a){
		this.socket.emit(a.tipo || "msg", {dest: a.dest, msg: a.msg});
	}
	discover(){
		return new Promise((resolver, rechazar)=>{
			this.socket.emit("discover");
			this.socket.on("discover2", b=>{
				this.socket.off("discover2");
				resolver(b);
			});
		});
	}
	terminar(){
		this.socket.disconnect();
	}
}
class Agenda{
	constructor(a){
		this.in=a;
	}
	get id(){
		return JSON.parse(window.vars.agenda)[a.nombre];
	}
	static añadir(a){
		window.vars.agenda=JSON.stringify(JSON.parse(window.vars.agenda[a.nombre]=a.id));
		return window.vars.agenda;
	}
}
